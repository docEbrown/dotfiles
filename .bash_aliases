# General
alias diff='diff --color=always'
alias tmn='tmux new-session'
alias tma='tmux attach'
alias tmls='tmux list-sessions'
alias df='df -x"squashfs"'

# Git
alias gitneo='git config --local user.email "gitlab@bjohnston.neomailbox.net" && git config --local user.signingkey 4AC3F71DB5A7674624B155B163E9F49BEDEC573D' 
alias githubneo='git config --local user.email "bjohnston@neomailbox.net" && git config --local user.signingkey C5767A9B3976B795EFDD38DA8728A554C2814E19' 
alias gitsyd='git config --local user.email "bjoh3944@uni.sydney.edu.au" && git config --local user.signingkey B79801C92841764C9AE94363FC2F813FD99D7DFF' 
alias gitrmd='git config --local user.email "ben.johnston@resmed.com.au" && git config --local user.signingkey 0AB4641309F7FBEB55F57915B836E0AB95F33FA0' 
alias gitec2='git config --local user.email "ben.johnston@resmed.com.au" && git config --local user.signingkey 0AB4641309F7FBEB55F57915B836E0AB95F33FA0' 

# VPN
alias rmd_vpn='sudo openconnect --juniper sslvpn.resmed.com.au'

# USYD
alias usyd_vpn='sudo openconnect vpn.sydney.edu.au'
alias hpc_login='ssh bjoh3944@hpc.sydney.edu.au'

# Dig Ocean
alias ben_cloud='ssh root@cloud.benjohnston.info'

# RMD aliases
alias lusee='ssh benj@172.21.254.20'
alias resgit='ssh benj@au2-devgit-d01.corp.resmed.org'
alias pipdproto='ssh ben@$PIPD_PROTO'
alias pipdext='ssh ben@$PIPD_EXT'
alias pipdsvc='ssh ben@$PIPD_SERVICES'
alias rhpc='ssh benj1@au2-pdehpc-p03.corp.resmed.org'
alias rghpc='ssh benj1@au2-pdehpc-p02.corp.resmed.org'
alias matbox='ssh pi@172.21.146.11'
alias raij_login='ssh bj7982@raijin.nci.org.au'
alias ux_coe1='ssh ec2-user@ec2-13-228-122-47.ap-southeast-1.compute.amazonaws.com' 
alias ux_coe2='ssh ec2-user@ec2-13-210-50-162.ap-southeast-2.compute.amazonaws.com' 
alias ux_coe3='ssh ec2-user@ec2-13-54-142-220.ap-southeast-2.compute.amazonaws.com' 
alias ux_coe4='ssh ec2-user@ec2-52-221-28-134.ap-southeast-1.compute.amazonaws.com'
alias ux_coe5='ssh ec2-user@ec2-54-206-96-18.ap-southeast-2.compute.amazonaws.com'
alias ux_coe6='ssh ec2-user@ec2-18-130-47-103.eu-west-2.compute.amazonaws.com'
alias ux_coe7='ssh ec2-user@ec2-35-164-188-177.us-west-2.compute.amazonaws.com'
alias ux_coe8='ssh ec2-user@ec2-52-35-210-122.us-west-2.compute.amazonaws.com'
alias bclock='ssh -i ~/.ssh/delorian_rmd ben@172.21.144.40'
alias netmount='sudo mount -t cifs --options credentials=/home/ben/.smbcredentials,uid=1000,gid=1000,file_mode=0775,dir_mode=0775,iocharset=utf8,rw //AU2FILE05/PD /home/ben/ProductDevelopment'
alias netmountvv='sudo mount -t cifs --options credentials=/home/ben/.smbcredentials,uid=1000,gid=1000,file_mode=0775,dir_mode=0775,iocharset=utf8,rw //AU2FILE05/VnVLab$ /home/ben/VnVLab'
alias pdsimnt='sudo mount -t cifs --options credentials=/home/ben/.smbcredentials,uid=1000,gid=1000,file_mode=0775,dir_mode=0775,iocharset=utf8,rw //AU2FILE02/pdsimulationdata /home/ben/ProductDevelopment'
alias netumount='sudo umount /home/ben/ProductDevelopment'
alias netumountvv='sudo umount /home/ben/VnVLab'
alias wacom='sudo xsetwacom set "Wacom Intuos S Pen stylus" Mode Relative'
alias tb='taskbook'
alias harrai='ssh ec2-user@52.62.98.55'
alias bengpu='ssh -i ~/.ssh/harrisonai.pub -o "IdentitiesOnly yes" ec2-user@ec2-52-37-197-219.us-west-2.compute.amazonaws.com'
